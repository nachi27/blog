class CommentsController < ApplicationController

	http_basic_authenticate_with :name => "nachiket", :password => "nachiket123", :only => :destroy
	
	def create
		@post = Post.find(params[:post_id])
		@comment = @post.comments.create(params[:comment])
		respond_to do |format|
	      format.html { redirect_to post_path(@post) }
	      format.js
	    end
	end

	def destroy
		@post = Post.find(params[:post_id])
		@comment = @post.comments.find(params[:id])
		@comment.destroy
		respond_to do |format|
	      format.html { redirect_to post_path(@post) }
	      format.js
	    end
	end
end
